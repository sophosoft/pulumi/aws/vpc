import { promise } from "."
import { small, large } from "./automation"
import { expect } from "chai"

describe("Small VPC Network", () => {
    it("VPC has the correct CIDR block", async () => {
        const cidr = await promise(small.vpc.cidrBlock)
        expect(cidr).to.equal('172.16.0.0/21')
    })

    it("VPC has the correct name", async () => {
        const tags = await promise(small.vpc.tags)
        expect(tags['Name']).to.equal('SMALL_OH')
    })

    it("IGW has the correct name", async () => {
        const tags = await promise(small.gateway.tags)
        expect(tags['Name']).to.equal('small-oh-igw')
    })

    it("VPC has a Route53 zone", async () => {
        const zone = await promise(small.dns.zone.name)
        expect(zone).to.equal('small.local')
    })

    it("Only uses 2 AZs", async () => {
        const network = small.networkSubnets
        expect(network.subnets.length).to.equal(2)
    })
})

describe("Large VPC Network", () => {
    it("VPC has NAT Gateways", async () => {
        expect(large.nats.length).to.equal(3)
    })

    it("VPC doesn't have a Route53 zone", async () => {
        expect(large.dns).to.be.undefined
    })

    it("VPC has an RDS SubnetGroup", async () => {
        const group = await promise(large.rdsSubnetGroup.name)
        expect(group).to.equal('large-oh-db-subnets')
    })

    it("VPC has an ElastiCache SubnetGroup", async () => {
        const group = await promise(large.ecSubnetGroup.name)
        expect(group).to.equal('large-oh-ec-subnets')
    })

    it("VPC has custom subnet CIDRs", async () => {
        const block = await promise(large.dataSubnets.subnets[2].subnet.cidrBlock)
        expect(block).to.equal("172.16.73.0/25")
    })
})
