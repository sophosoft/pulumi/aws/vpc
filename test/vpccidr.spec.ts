import { VpcCidr } from "../src/VpcCidr"
import { expect } from "chai"


describe("VpcCidr", () => {
  const cidrBlock: string = "172.16.0.0/21"
  const azList: string[] = ["us-east-2a", "us-east-2b", "us-east-2c"]
  let vpcCidr: VpcCidr

  before(() => {
    vpcCidr = new VpcCidr(cidrBlock, azList)
  })

  it("calculates internal subnets", () => {
    const blocks = [
      { zone: 'us-east-2a', type: 'internal', cidr: '172.16.0.0/24' },
      { zone: 'us-east-2b', type: 'internal', cidr: '172.16.2.0/24' },
      { zone: 'us-east-2c', type: 'internal', cidr: '172.16.4.0/24' }
    ]
    expect(vpcCidr.internalBlocks).to.deep.equal(blocks)
  })

  it("calculates external subnets", () => {
    const blocks = [
      { zone: 'us-east-2a', type: 'external', cidr: '172.16.1.0/25' },
      { zone: 'us-east-2b', type: 'external', cidr: '172.16.3.0/25' },
      { zone: 'us-east-2c', type: 'external', cidr: '172.16.5.0/25' }
    ]
    expect(vpcCidr.externalBlocks).to.deep.equal(blocks)
  })

  it("calculates data subnets", () => {
    const blocks = [
      { zone: 'us-east-2a', type: 'data', cidr: '172.16.1.128/26' },
      { zone: 'us-east-2b', type: 'data', cidr: '172.16.3.128/26' },
      { zone: 'us-east-2c', type: 'data', cidr: '172.16.5.128/26' }
    ]
    expect(vpcCidr.dataBlocks).to.deep.equal(blocks)
  })

  it("calculates network subnets", () => {
    const blocks = [
      { zone: 'us-east-2a', type: 'network', cidr: '172.16.1.192/26' },
      { zone: 'us-east-2b', type: 'network', cidr: '172.16.3.192/26' },
      { zone: 'us-east-2c', type: 'network', cidr: '172.16.5.192/26' }
    ]
    expect(vpcCidr.networkBlocks).to.deep.equal(blocks)
  })
})
