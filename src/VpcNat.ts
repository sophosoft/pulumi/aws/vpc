import { ComponentResource, Input, ComponentResourceOptions } from "@pulumi/pulumi";
import { Eip, NatGateway, RouteTable } from "@pulumi/aws/ec2";

export interface VpcNatOptions {
    azLetter: string,
    namePrefix: string,
    subnetId: string | Input<string>,
    vpcId: string | Input<string>
}

export class VpcNat extends ComponentResource {
    public readonly eip: Eip
    public readonly nat: NatGateway
    public readonly routes: RouteTable

    constructor(name: string, options: VpcNatOptions, extra?: ComponentResourceOptions) {
        super('sophosoft:aws:VpcNat', name, {}, extra)

        this.eip = new Eip(`${name}-eip`, {
            vpc: true,
            tags: {
                Name: `${options.namePrefix}-eip-${options.azLetter}`
            }
        }, { ...extra, parent: this })

        this.nat = new NatGateway(`${name}-nat`, {
            allocationId: this.eip.id,
            subnetId: options.subnetId,
            tags: {
                Name: `${options.namePrefix}-nat-${options.azLetter}`
            }
        }, { ...extra, parent: this })

        this.routes = new RouteTable(`${name}-routes`, {
            vpcId: options.vpcId,
            routes: [{
                cidrBlock: '0.0.0.0/0',
                natGatewayId: this.nat.id
            }],
            tags: {
                Name: `${options.namePrefix}-internal-${options.azLetter}`
            }
        }, { ...extra, parent: this })
    }
}
