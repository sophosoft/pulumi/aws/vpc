import { ComponentResource, ComponentResourceOptions, Input } from "@pulumi/pulumi";
import { VpcSubnet, VpcSubnetType } from "./VpcSubnet";
import { RouteTable } from "@pulumi/aws/ec2";
import { VpcSubnetCidrBlock } from "./VpcCidr";

/**
 * Configuration options for the VpcSubnetGroup component
 */
export interface VpcSubnetGroupOptions {
    namePrefix: string
    routeTables: RouteTable[]
    subnetCidrBlocks: VpcSubnetCidrBlock[]
    type: VpcSubnetType
    vpcId: string | Input<string>
}

/**
 * A VpcSubnetGroup is a collection of subnets across multiple AZs that share a common purpose,
 * such as for data resources like caches or databases.  They'll have similar CIDRs, incremented
 * by the defined `blockSize`.
 */
export class VpcSubnetGroup extends ComponentResource {
    public readonly subnets: VpcSubnet[] = []
    public readonly type: VpcSubnetType

    constructor(name: string, options: VpcSubnetGroupOptions, extra?: ComponentResourceOptions) {
        super('sophosoft:aws:VpcSubnetGroup', name, {}, extra)

        this.type = options.type

        options.subnetCidrBlocks.map((block: VpcSubnetCidrBlock, index: number) => {
            let table = options.routeTables[index] || options.routeTables[0]

            this.subnets.push(new VpcSubnet(`${name}-${index + 1}`, {
                availabilityZone: block.zone,
                cidrBlock: block.cidr,
                routeTableId: table.id,
                type: this.type,
                vpcId: options.vpcId,
                namePrefix: options.namePrefix
            }, { ...extra, parent: this }))
        })

        this.registerOutputs({
            subnets: this.subnets,
            type: this.type
        })
    }
}
