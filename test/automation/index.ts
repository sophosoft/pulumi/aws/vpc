import { VpcNetwork } from "../../src/index"
import { runTests } from "../index"

export const small = new VpcNetwork('small', {
    cidrBlock: "172.16.0.0/21",
    maxZones: 2,
    name: 'SMALL_OH',
    dnsOptions: {
        domain: 'small.local'
    }
})

export const large = new VpcNetwork('large', {
    cidrBlock: "172.16.32.0/19",
    name: 'LARGE_OH',
    subnetOptions: {
        azAdditionalBits: 1,
        subnetAdditionalBits: [1, 3, 1, 0]
    }
})

runTests()
