import { VpcSubnetType } from "./VpcSubnet";
import { cidrsubnet, cidrsubnets } from "@sophosoft/hard-cidr";

export interface VpcZoneCidrBlock {
  zone: string
  cidr: string
}

export interface VpcSubnetCidrBlock {
  zone: string
  type: VpcSubnetType
  cidr: string
}

export class VpcCidr {
  public readonly dataBlocks:     VpcSubnetCidrBlock[] = []
  public readonly externalBlocks: VpcSubnetCidrBlock[] = []
  public readonly internalBlocks: VpcSubnetCidrBlock[] = []
  public readonly networkBlocks:  VpcSubnetCidrBlock[] = []

  constructor(
    public readonly cidrBlock: string,
    public readonly azList: string[],
    zoneAdditionalBits: number = 2,
    subnetAdditionalBits: number[] = [1, 1, 1, 0]
  ) {
    const zoneBlocks: VpcZoneCidrBlock[] = azList.map((zone: string, i: number): VpcZoneCidrBlock => {
      return { zone, cidr: cidrsubnet(cidrBlock, zoneAdditionalBits, i) }
    })

    zoneBlocks.map((block: VpcZoneCidrBlock) => {
      const subnetBlocks = cidrsubnets(block.cidr, subnetAdditionalBits)
      this.dataBlocks.push({ zone: block.zone, type: "data", cidr: subnetBlocks[2] })
      this.externalBlocks.push({ zone: block.zone, type: "external", cidr: subnetBlocks[1] })
      this.internalBlocks.push({ zone: block.zone, type: "internal", cidr: subnetBlocks[0] })
      this.networkBlocks.push({ zone: block.zone, type: "network", cidr: subnetBlocks[3] })
    })
  }
}
