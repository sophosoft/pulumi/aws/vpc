# AWS VPC Component

VPCs are fairly complex resources with many different components.  This aggregates most of them according to best-practices in a simplified, but flexible manner.

> This component will provision NAT Gateways for each Availability Zone, which will incur a recurring cost.

## install

```sh
npm i @sophosoft/pulumi-aws-vpc
```

## usage

```ts
import { VpcNetwork } from "@sophosoft/pulumi-aws-vpc"

export const small = new VpcNetwork('small', {
  cidrBlock: "172.16.0.0/21",
  maxZones: 2,
  name: 'SMALL_EAST_2',
  dnsOptions: {
    domain: 'small.local'
  }
})

export const large = new VpcNetwork('large', {
  cidrBlock: "172.16.32.0/19",
  name: 'LARGE_WEST_2',
  subnetOptions: {
    azAdditionalBits: 1,
    subnetAdditionalBits: [1, 3, 1, 0]
  }
})
```

## outputs

### VpcNetwork

- vpc: `aws.ec2.Vpc`
- gateway: `aws.ec2.InternetGateway`
- externalRoutes: `aws.ec2.RouteTable`
- internalRoutes: `aws.ec2.RouteTable[]`
- dns: `VpcDns`
- nats: `VpcNat`
- externalSubnets: `VpcSubnetGroup`
- internalSubnets: `VpcSubnetGroup`
- dataSubnets: `VpcSubnetGroup`
- networkSubnets: `VpcSubnetGroup`
- rdsSubnetGroup: `aws.rds.SubnetGroup`
- ecSubnetGroup: `aws.elasticache.SubnetGroup`

### VpcDns

- zone: `aws.route53.Zone`
- dhcp: `aws.ec2.VpcDhcpOptions`
- association: `aws.ec2.VpcDhcpOptionsAssociation`

### VpcNat

- eip: `aws.ec2.Eip`
- nat: `aws.ec2.NatGateway`
- routes: `aws.ec2.RouteTable`

### VpcSubnetGroup

- subnets: `VpcSubnet[]`
- type: `VpcSubnetType`

#### VpcSubnetType

- `external`
- `internal`
- `data`
- `network`

### VpcSubnet

- subnet: `aws.ec2.Subnet`
- association?: `aws.ec2.RouteTableAssociation`
