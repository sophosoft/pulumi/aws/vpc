import { ComponentResource, ComponentResourceOptions } from "@pulumi/pulumi"
import { Vpc, InternetGateway, RouteTable } from "@pulumi/aws/ec2"
import { getAvailabilityZones } from "@pulumi/aws"
import { SubnetGroup as RdsSubnetGroup } from "@pulumi/aws/rds"
import { SubnetGroup as EcSubnetGroup } from "@pulumi/aws/elasticache"
import { VpcDns } from "./VpcDns"
import { VpcSubnetGroup } from "./VpcSubnetGroup"
import { VpcSubnet } from "./VpcSubnet"
import { VpcNat } from "./VpcNat"
import { VpcCidr } from "./VpcCidr"

/**
 * DNS configuration options for a `VpcNetwork`
 */
export interface VpcNetworkDnsOptions {
    domain: string
    nameServers?: string[]
    region?: string
}

/**
 * Configuration options for a `VpcNetwork`
 */
export interface VpcNetworkOptions {
    cidrBlock: string
    dnsOptions?: VpcNetworkDnsOptions
    maxZones?: number
    name: string
    subnetOptions?: VpcNetworkSubnetOptions
}

export interface VpcNetworkSubnetOptions {
    azAdditionalBits?: number
    subnetAdditionalBits?: number[]
}

/**
 * A `VpcNetwork` provides multiple resources to establish a consistent baseline for logical
 * networking organization.
 */
export class VpcNetwork extends ComponentResource {
    /**
     * The `VPC`
     */
    public readonly vpc: Vpc

    /**
     * An `InternetGateway` for external communication
     */
    public readonly gateway: InternetGateway

    /**
     * A `RouteTable` for public facing resources
     */
    public readonly externalRoutes: RouteTable

    /**
     * `RouteTables` for internal resources
     */
    public readonly internalRoutes: RouteTable[] = []

    /**
     * (optional) DNS resources
     */
    public readonly dns?: VpcDns

    /**
     * `NatInstances` and their associated `RouteTables`
     * @see internalRoutes
     */
    public readonly nats: VpcNat[] = []

    /**
     * Subnets for externally facing resources
     */
    public readonly externalSubnets: VpcSubnetGroup

    /**
     * Subnets used for internal, or private, resources only
     */
    public readonly internalSubnets: VpcSubnetGroup

    /**
     * Subnets for data resources, like caches and databases
     */
    public readonly dataSubnets: VpcSubnetGroup

    /**
     * Subnets for networking resources, such as domain controllers 
     */
    public readonly networkSubnets: VpcSubnetGroup

    /**
     * Default `SubnetGroup` for RDS
     */
    public readonly rdsSubnetGroup: RdsSubnetGroup

    /**
     * Default `SubnetGroup` for ElastiCache
     */
    public readonly ecSubnetGroup: EcSubnetGroup

    protected namePrefix: string

    constructor(name: string, options: VpcNetworkOptions, extra?: ComponentResourceOptions) {
        super('sophosoft:aws:VpcNetwork', name, {}, extra)
        this.namePrefix = options.name.toLowerCase().replace('_', '-')

        this.vpc = new Vpc(`${name}-vpc`, {
            cidrBlock: options.cidrBlock,
            enableDnsHostnames: (options.dnsOptions !== null),
            enableDnsSupport: (options.dnsOptions !== null),
            tags: {
                Name: options.name
            }
        }, { ...extra, parent: this })

        this.gateway = new InternetGateway(`${name}-igw`, {
            vpcId: this.vpc.id,
            tags: {
                Name: `${this.namePrefix}-igw`
            }
        }, { ...extra, parent: this })

        this.externalRoutes = new RouteTable(`${name}-external`, {
            vpcId: this.vpc.id,
            routes: [{
                cidrBlock: '0.0.0.0/0',
                gatewayId: this.gateway.id
            }],
            tags: {
                Name: `${this.namePrefix}-external`
            }
        }, { ...extra, parent: this })

        if (options.dnsOptions) {
            this.dns = new VpcDns(`${name}-dns`, {
                domainName: options.dnsOptions.domain,
                nameServers: options.dnsOptions.nameServers,
                prefix: this.namePrefix,
                region: options.dnsOptions.region,
                vpcId: this.vpc.id,
            }, { ...extra, parent: this })
        }

        const azList = getAvailabilityZones({ state: 'available' }).names.slice(0, options.maxZones || 3)
        const vpcCidr = new VpcCidr(
            options.cidrBlock,
            azList,
            options.subnetOptions?.azAdditionalBits,
            options.subnetOptions?.subnetAdditionalBits
        )

        this.externalSubnets = new VpcSubnetGroup(`${name}-subnets-external`, {
            namePrefix: this.namePrefix,
            subnetCidrBlocks: vpcCidr.externalBlocks,
            routeTables: [this.externalRoutes],
            type: 'external',
            vpcId: this.vpc.id
        }, { ...extra, parent: this })

        this.externalSubnets.subnets.forEach((subnet: VpcSubnet, index: number) => {
            const nat = new VpcNat(`${name}-nat-${index + 1}`, {
                azLetter: azList[index][azList[index].length - 1],
                namePrefix: this.namePrefix,
                subnetId: subnet.subnet.id,
                vpcId: this.vpc.id
            }, { ...extra, parent: this })
            this.nats.push(nat)
            this.internalRoutes.push(nat.routes)
        })

        this.internalSubnets = new VpcSubnetGroup(`${name}-subnets-internal`, {
            namePrefix: this.namePrefix,
            subnetCidrBlocks: vpcCidr.internalBlocks,
            routeTables: this.internalRoutes,
            type: 'internal',
            vpcId: this.vpc.id
        }, { ...extra, parent: this })

        this.dataSubnets = new VpcSubnetGroup(`${name}-subnets-data`, {
            namePrefix: this.namePrefix,
            subnetCidrBlocks: vpcCidr.dataBlocks,
            routeTables: this.internalRoutes,
            type: 'data',
            vpcId: this.vpc.id
        }, { ...extra, parent: this })

        this.rdsSubnetGroup = new RdsSubnetGroup(`${name}-subnetGroup-rds`, {
            name: `${this.namePrefix}-db-subnets`,
            description: `Data Subnets for ${options.name}`,
            subnetIds: this.dataSubnets.subnets.map((sn: VpcSubnet) => sn.subnet.id),
            tags: {
                Name: `${this.namePrefix}-db-subnets`
            }
        }, { ...extra, parent: this })

        this.ecSubnetGroup = new EcSubnetGroup(`${name}-subnetGroup-ec`, {
            name: `${this.namePrefix}-ec-subnets`,
            description: `Data Subnets for ${options.name}`,
            subnetIds: this.dataSubnets.subnets.map((sn: VpcSubnet) => sn.subnet.id)
        }, { ...extra, parent: this })

        this.networkSubnets = new VpcSubnetGroup(`${name}-subnets-network`, {
            namePrefix: this.namePrefix,
            subnetCidrBlocks: vpcCidr.networkBlocks,
            routeTables: this.internalRoutes,
            type: 'network',
            vpcId: this.vpc.id
        }, { ...extra, parent: this })

        this.registerOutputs({
            vpc: this.vpc,
            gateway: this.gateway,
            externalRoutes: this.externalRoutes,
            internalRoutes: this.internalRoutes,
            dns: this.dns,
            nats: this.nats,
            externalSubnets: this.externalSubnets,
            internalSubnets: this.internalSubnets,
            dataSubnets: this.dataSubnets,
            networkSubnets: this.networkSubnets,
            rdsSubnetGroup: this.rdsSubnetGroup,
            ecSubnetGroup: this.ecSubnetGroup
        })
    }
}
