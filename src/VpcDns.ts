import { ComponentResource, Input, ComponentResourceOptions } from "@pulumi/pulumi";
import { Zone } from "@pulumi/aws/route53";
import { VpcDhcpOptions, VpcDhcpOptionsAssociation } from "@pulumi/aws/ec2";

export interface VpcDnsOptions {
    domainName: string,
    nameServers?: string[],
    prefix: string
    region?: string
    vpcId: string | Input<string>
}

export class VpcDns extends ComponentResource {
    public readonly zone: Zone
    public readonly dhcp: VpcDhcpOptions
    public readonly association: VpcDhcpOptionsAssociation

    constructor(name: string, options: VpcDnsOptions, extra?: ComponentResourceOptions) {
        super('sophosoft:aws:VpcDns', name, {}, extra)

        this.zone = new Zone(`${name}-zone`, {
            vpcs: [{
                vpcId: options.vpcId,
                vpcRegion: options.region
            }],
            name: options.domainName
        }, { ...extra, parent: this })

        this.dhcp = new VpcDhcpOptions(`${name}-dhcp`, {
            domainName: options.domainName,
            domainNameServers: options.nameServers || ['AmazonProvidedDNS'],
            tags: {
                Name: `${options.prefix}-dhcp`
            }
        }, { ...extra, parent: this })

        this.association = new VpcDhcpOptionsAssociation(`${name}-dhcp`, {
            dhcpOptionsId: this.dhcp.id,
            vpcId: options.vpcId
        }, { ...extra, parent: this })
    }
}
