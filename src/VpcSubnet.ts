import { ComponentResource, Input, ComponentResourceOptions } from "@pulumi/pulumi"
import { Subnet, RouteTableAssociation } from "@pulumi/aws/ec2"

export type VpcSubnetType = 'internal' | 'external' | 'data' | 'network'

export interface VpcSubnetOptions {
    vpcId: string | Input<string>
    availabilityZone: string
    cidrBlock: string
    namePrefix: string
    type: VpcSubnetType
    routeTableId?: string | Input<string>
}

export class VpcSubnet extends ComponentResource {
    public readonly subnet: Subnet
    public readonly association?: RouteTableAssociation

    constructor(name: string, options: VpcSubnetOptions, extra?: ComponentResourceOptions) {
        super('sophosoft:aws:VpcSubnet', name, {}, extra)

        const zoneLetter = options.availabilityZone[options.availabilityZone.length - 1]

        this.subnet = new Subnet(`${name}-subnet`, {
            vpcId: options.vpcId,
            availabilityZone: options.availabilityZone,
            mapPublicIpOnLaunch: (options.type === 'external'),
            cidrBlock: options.cidrBlock,
            tags: {
                Name: `${options.namePrefix}-${options.type}-${zoneLetter}`
            }
        }, { ...extra, parent: this })

        if (options.routeTableId !== undefined) {
            this.association = new RouteTableAssociation(`${name}-rta`, {
                routeTableId: options.routeTableId,
                subnetId: this.subnet.id
            }, { ...extra, parent: this })
        }
    }
}
